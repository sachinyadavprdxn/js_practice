window.onload=function(){

	var xValue = 0;
  var yValue = -68;
  (function(){
		document.getElementById("layer-1").style.backgroundPosition = xValue+"px "+yValue+"px";
  	document.getElementById("layer-2").style.backgroundPosition = xValue+"px "+yValue+"px";
  	document.getElementById("layer-3").style.backgroundPosition = xValue+"px "+yValue+"px";
	})();

	window.addEventListener("scroll", parallax, false);
	function parallax(){
		 	var ypos = pageYOffset;
		 	var slide1 = document.getElementById('slide1');
		 	slide1.style.top = ypos * .6 + 'px';
	}

	var strength1 = 8;
	var strength2 = 15;
	var strength3 = 30;
	document.getElementById("slide1").addEventListener("mousemove", function(e){
	    var pageX = e.pageX - ($(window).width() / 2);
	    var pageY = e.pageY - ($(window).height() / 2);
	    document.getElementById("layer-1").style.backgroundPosition = (strength1 / $(window).width() * pageX * -1)+"px "+(strength1  / $(window).height() * pageY * -1 + yValue)+"px";
	    document.getElementById("layer-2").style.backgroundPosition = (strength2 / $(window).width() * pageX * -1)+"px "+(strength2  / $(window).height() * pageY * -1 + yValue)+"px";
	    document.getElementById("layer-3").style.backgroundPosition = (strength3 / $(window).width() * pageX * -1)+"px "+(strength3  / $(window).height() * pageY * -1 + yValue)+"px";
	});
};
