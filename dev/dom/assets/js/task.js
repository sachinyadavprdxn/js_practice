setInterval(function myFunction(){
  var parent = document.getElementById('bkc');
  var list = parent.getElementsByTagName('LI');
  //console.log(list.length);
  for (var i = 1; i < list.length; i = i + 2)
    {
      cls = list[i].className;
      if (cls !== 'red')
        {
          list[i].innerHTML = 'Hello';
          list[i].classList.add('red');
        }
      else
        {
          list[i].innerHTML = [i + 1];
          list[i].classList.remove('red');
        }
    }
 },2000);


<!-- /*============Replace the <li> having specified class name("remove") by new <li> ==============*/ -->
setInterval(function(){
$(".remove").replaceWith("<li>This is the remove class element</li>");
}, 3000);

<!-- /*================Chang Background of Div that not having class name "selected"===============*/ -->
$(document).ready(function(){
	$( "#backg div" ).not( ".selected" ).css( "background-color", "#0F0" );
});

<!-- /*===function that will discard visibility: hidden and display:none property from the elements ==*/ -->
setInterval(function(){
$(".select").toggleClass("visibal");
  $('.any').css('display', 'block');
}, 3000);

<!-- /*==function range that takes one argument, a positive number, and returns an array containing all numbers from 0 up to and including the given number. ==*/ -->
document.getElementById('range').addEventListener("click", range);
arr = [];
function range(){
  var num = document.getElementById('norang').value;
  
  for(var i = 0; i <= num; i++){
    arr[i] = i;
    
  }
  document.getElementById("res").innerHTML = arr;
 }

 <!-- /*==function range that takes one argument, a positive number, and returns an array containing all numbers from 0 up to and including the given number. ==*/ -->
document.getElementById('range').addEventListener("click", range);
arr = [];
function range(){
  var num = document.getElementById('norang').value;
  
  for(var i = 0; i <= num; i++){
    arr[i] = i;
    
  }
  document.getElementById("res").innerHTML = arr;
 }

 <!-- /*==function that will check whether password and confirm password is matching or not and password should not be blank. ==*/ -->

$('#check').on('click', function () {
  if (($('#pass').val() == '') && ($('#cpass').val() == '')) {
      $('#err, #err').html('Please Enter Password').css('color', 'red');
  } 
  else if ($('#pass').val() == '') {
      $('#err').html('Please Enter Password').css('color', 'red');
  } 
  else if ($('#cpass').val() == '') {
      $('.err').html('Please Enter Password').css('color', 'red');
  }
  else if ($('#cpass').val() == $('#pass').val()) {
      $('#msg').html('Accepted').css('color', 'green');
  }
  else $('.err').html('not matching').css('color', 'red');
  
  $('#pass').click(function(){
    $('#err').html(" ");
  });

  $('#cpass').click(function(){
    $('.err').html(" ");
  });
});

<!-- /*==program to check whether string is 'palindrome' or not ==*/ -->
$(document).ready(function(){
  $('#palin-check').on('click', function () {
    var str = $('#palin').val();
    var revrs = str.split("").reverse().join("");
    if (str == revrs) {
      $('#masg').html('String is Palindrome').css('background-color', 'green');
    }
    else {
      $('#masg').html('String is  not an Palindrome').css('background-color', 'red');
    }
  });
});

<!-- /*==program to filter array containing names of students or places starting with "J". ==*/ -->
var strArray = ['Narendra', 'Tejas', 'Sachin', 'Shreyas', 'Shriney', 'Neha'];

document.getElementById('submit').addEventListener("click", searchStringInArray);
var newArr = [];
function searchStringInArray() {
  var str = document.getElementById('name').value;
  if (str === '') {
    document.getElementById('error').innerHTML = 'Please Enter The String';
  }
  else {
    var j = 0;
    for (var i = 0; i < strArray.length; i++) {
      if (strArray[i].charAt(0) == str.charAt(0)) {
        newArr[j] = strArray[i];
        j++;
      }
    }
    document.getElementById('original').innerHTML = 'Original Array Is: ' +strArray;
    document.getElementById('new').innerHTML = 'New Array Is: ' +newArr;
  }  
}

<!-- /*==write a program to set attribute href and its value to the anchors having class 'add' and remove attribute from anchors having class name'remove'. ==*/ -->
$( '#add-remove' ).click( function () {
 
  if ($('.add').attr('href')) {
    $(".add").removeAttr("href");
  } 
  else {
    $('.add').attr('href', 'FIXME');
 } 
  
  if ($('.removed').attr('href')) {
    $(".removed").removeAttr("href");
  } 
  else {
    $('.removed').attr('href', 'FIXME');
  }
});


function slideSwitch() {
    var $active = $('.img-anim img.active');
    var $next = $active.next();    
    
    $next.addClass('active');
    
    $active.removeClass('active');
}

$(function() {
    setInterval( "slideSwitch()", 1000 );
});
