function stopEvent(ev) {
  c2 = document.getElementById("c2");
  c2.innerHTML = "hello";

  // this ought to keep t-daddy from getting the click.
  ev.stopPropagation();
  alert("event propagation halted.");
}

function load() {
  elem = document.getElementById("tbl1");
  elem.addEventListener("click", stopEvent, false);
}

/*===================preventDefault===========================*/
function stopDefAction(evt) {
    evt.preventDefault();
}
    
document.getElementById('my-checkbox').addEventListener(
    'click', stopDefAction, false
);

/*===================Manipulating Styles===========================*/
