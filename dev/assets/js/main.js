	// var price1 = 5;
	// var price2 = 6;
	// var total = price1 + price2;
	// var person = "John Doe", carName = "Volvo", price = 200;
	// document.getElementById("var").innerHTML ="The total is: " + total;
	// var person = "John Doe", carName = "Volvo", price = 200;
	// document.write(carName);
	// document.write("<br />");

	// var myVar = "global"; // Declare a global variable
	// function checkscope( ) {
	//    var myVar = "local";  // Declare a local variable
	//    document.write(myVar);
	//    document.write("<br /><br />");
	// }
	// checkscope( );

	//var x = 16 + "Volvo";
	//var x = "16" + "Volvo";
	//var x = "16" + Volvo;
	//var x = 16 + Volvo;
	//var x = 16 + "4";
	//var x = 16 + 4 + "Volvo";
	//var x = "Volvo" + 16 + 4;
// 	var x = "Volvo" + (16 + 4);
// 	document.getElementById("datatype").innerHTML = x;

// 	var pi = 3.14;
// 	var person = "John Doe";
// 	var answer = 'Yes I am!';

// 	//document.getElementById("datatype").innerHTML = pi;
// 	document.getElementById("datatype").innerHTML = person;
// 	//document.getElementById("datatype").innerHTML = answer;


// document.write("***Operators*****<br /><br />");
// 	var A = 10;
// 	var B = 20;
// 	var C = 20;
// 	var D = 20;
// 	if (A == B)
// 	{
// 		document.write("True <br>");
// 	}
// 	//else if (A !== B)
// 	//{
// 		//document.write("False");
// 	//}
// 	else if (A > B)
// 	{
// 		document.write("A is greater than B <br />");
// 	}
// 	else if (A < B)
// 	{
// 		document.write("A is smaller than B <br />");
// 	}
// 	if (C >= D)
// 	{
// 		document.write("C is greater than equal to D <br />");
// 	}
// 	else if (C <= D)
// 	{
// 		document.write("C is smaller than equal to D <br />");
// 	}

// 	var A = 10;
// 	var B = 20;
// 	var C = 10;
// 	var D = 20;
// 	if ((A > 11) && (B > 11))
// 	{
// 		document.write("True <br />");
// 	}
// 	else 
// 	{
// 		document.write("False <br />");
// 	}

// 	if ((C > 11) || (D > 11)) 
// 	{
// 		document.write("True <br />");
// 	}
// 	else 
// 	{
// 		document.write("False <br />");
// 	}

// 	if (!((A > 11) && (B > 11))) 
// 	{
// 		document.write("True<br />");
// 	}
// 	else 
// 	{
// 		document.write("False<br />");
// 	}

// 	var A = 9;
// 	var B = 12;
// 	var C = 2;
					
// 	if ((A > B) && (A > C)) 
// 	{
// 	  document.write("A is greater than B and C<br />");
// 	}
// 	else if ((B > A) && (B > C)) 
// 	{
// 	  document.write("B is greater than A and C<br /><br />");
// 	}
// 	else 
// 	{
// 	  document.write("C is greater than A and B<br />");
// 	}

// 	document.write("***Switch case*****<br />");
// 	var grade = 'B';
// 	switch (grade)
// 	{
// 	  case 'A': document.write("Distinction<br />");
// 	            break;
// 	  case 'B': document.write("First Class<br /><br />");
// 	            break;
// 	  case 'C': document.write("Second Class<br />");
// 	            break;
// 	  case 'D': document.write("Pass Class<br />");
// 	            break;
// 	  case 'F': document.write("Failed<br />");
// 	            break;
// 	  default:  document.write("Unknown grade<br />");
// 	}

// 	var grade = 'B';
// 	switch (grade)
// 	{
// 	  case 'A': document.write("Marks obtained bitween 75 and 100<br />");
// 	            break;
// 	  case 'B': document.write("Marks obtained bitween 60 and 75<br /><br />");
// 	            break;
// 	  case 'C': document.write("Marks obtained bitween 50 and 60<br />");
// 	            break;
// 	  case 'D': document.write("Marks obtained bitween 40 and 50<br />");
// 	            break;
// 	  case 'F': document.write("Failed<br />");
// 	            break;
// 	  default:  document.write("Unknown grade<br />");
// 	}

// document.write("***For Loop*****<br />");

// 	for(var i = 0; i <= 5; i++) 
// 	{
// 		for(var x = 0; x <= i; x++)
// 		{
// 			document.write("*");
// 		}
// 		document.write("<br/>"); 
// 	}

// 	for(var i = 1; i <= 10; i += 2) 
// 	{
// 	  for (var k = 0; k < 4 - i / 2; k++)
// 	    {
// 	      document.write("&nbsp;");
// 	    }
// 		for(var x = 0; x < i; x++)
// 		{
// 		  document.write("*");
// 		}
// 		document.write("<br/>"); 
// 	}

// 	for(var i = 10; i >= 0; i -= 2) 
// 	{
// 	  for (var k = 0; k <= 4 - i / 2; k++)
// 	    {
// 	      document.write("&nbsp;");
// 	    }
// 		for(var x = 0; x <= i; x++)
// 		{
// 		  document.write("*");
// 		}
// 		document.write("<br/>"); 
// 	}

// document.write("<br/>"); 
// 	//window.alert(5 + 6);

// document.write("***While Loop*****<br />");
// var count = 0;
// document.write("Starting Loop" + "<br />");
// while (count < 10)
// {
//   document.write("Current Count : " + count + "<br />");
//   count++;
// }
// document.write("Loop stopped!");

// document.write("<br /><br />***Loop control*****<br />");

// document.write("Entering the loop!<br /> ");
// outerloop:   // This is the label name
// for (var i = 0; i < 5; i++)
// {
//   document.write("Outerloop: " + i + "<br />");
//   innerloop:
//   for (var j = 0; j < 5; j++)
//   {
//      if (j >  3 ) break ;         // Quit the innermost loop
//      if (i == 2) break innerloop; // Do the same thing
//      if (i == 3) break outerloop; // Quit the outer loop
//      document.write("Innerloop: " + j + "  <br />");
//    }
// }
// document.write("Exiting the loop!<br /> ");

// function clear()
// {
// 	var a = document.getElementById("no1").value;
// 	var b = document.getElementById("no2").value;
// 	alert (a);
// 	if (a !== '' && b !== '') 
// 		{
			
// 		};
// }

/********Calculator**********/
function add()
{
	var a = parseInt(document.getElementById("no1").value);
	var b = parseInt(document.getElementById("no2").value);
	var c = a + b;
  
	var para = document.createElement("P");
  var t = document.createTextNode("Addition of " +a+ "+" +b+ " is: " +c);
  para.appendChild(t);
  document.body.appendChild(para);  
}

function sub()
{
	var a = parseInt(document.getElementById("no1").value);
	var b = parseInt(document.getElementById("no2").value);
	var c = a - b;
	var para = document.createElement("P");
  var t = document.createTextNode("Substraction of " +a+ "-" +b+ " is: " +c);
  para.appendChild(t);
  document.body.appendChild(para);   
}

function mul()
{
	var a = parseInt(document.getElementById("no1").value);
	var b = parseInt(document.getElementById("no2").value);
	var c = a * b;
  
	var para = document.createElement("P");
  var t = document.createTextNode("Multiplication of " +a+ "*" +b+ " is: " +c);
  para.appendChild(t);
  document.body.appendChild(para);    
}

function div()
{
	var a = parseInt(document.getElementById("no1").value);
	var b = parseInt(document.getElementById("no2").value);
	var c = a / b;

	var para = document.createElement("P");
  var t = document.createTextNode("Division of " +a+ "/" +b+ " is: " +c);
  para.appendChild(t);
  document.body.appendChild(para);
}

