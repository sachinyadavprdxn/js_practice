window.onload=function(){

	document.getElementById('submit').addEventListener('click', function (e){
		vali_name();
		email_name();
		tele_num();
		pin_code();
		user_name();
		vali_password();
		vali_Cpassword();
		vali_cqbox();

		if(flag1 || flag2 || flag3 || flag4 || flag5 || flag6 || flag7 || flag8) {
      //document.getElementById('registration_form').submit();
      alert("You Register succesfully");
			location.reload();
      
    } 
    else {
    	vali_name();
			email_name();
			tele_num();
			pin_code();
			user_name();
			vali_password();
			vali_Cpassword();
			vali_cqbox();
			e.preventDefault();
    }
	});

	var fname = document.getElementById('name');
	var emailname = document.getElementById('email');
	var tname = document.getElementById('mobile');
	var pcode = document.getElementById('pincode');
	var uname = document.getElementById('username');
	var pass = document.getElementById('password');
	var cpass = document.getElementById('conf-password');
	var chqbox = document.getElementById('check-box');

	/* On Blur Call for validation Function */
	fname.addEventListener('blur', function() {
    vali_name();
	});

	emailname.addEventListener('blur', function() {
    email_name();
	});

	tname.addEventListener('blur', function() {
    tele_num();
	});

	pcode.addEventListener('blur', function() {
    pin_code();
	});

	uname.addEventListener('blur', function() {
    user_name();
	});

	pass.addEventListener('blur', function() {
    vali_password();
	});

	cpass.addEventListener('blur', function() {
    vali_Cpassword();
	});

	/* On Focus Remove Error Text */

	fname.addEventListener('focus', function() {
	  document.getElementById('nameerror').innerHTML = '';
	});

	emailname.addEventListener('focus', function() {
	  document.getElementById('emailerror').innerHTML = '';
	});

	tname.addEventListener('focus', function() {
	  document.getElementById('moberror').innerHTML = '';
	});

	pcode.addEventListener('focus', function() {
	  document.getElementById('pinerror').innerHTML = '';
	});

	uname.addEventListener('focus', function() {
	  document.getElementById('usererror').innerHTML = '';
	});

	pass.addEventListener('focus', function() {
	  document.getElementById('passerror').innerHTML = '';
	});

	cpass.addEventListener('focus', function() {
	  document.getElementById('cpasserror').innerHTML = '';
	});

	chqbox.addEventListener('focus', function() {
	  document.getElementById('checkerror').innerHTML = '';
	});

	var flag1 = true,
	flag2 = true,
	flag3 = true,
	flag4 =true,
	flag5 = true,
	flag6 = true,
	flag7 = true,
	flag8 = true;

//Function for Name validation
	function vali_name(){
	  var fname = document.getElementById('name').value;
	  var flen = fname.length;
	  var name_pattern = /^[a-zA-Z\s\.\-]+[a-zA-Z\.\']+$/;
	  if (fname==null || fname=="" ){
	  	flag1 = false;
	    document.getElementById('nameerror').innerHTML = 'Please Enter The Name.'; 
	  }
	  else if(!name_pattern.test(fname) || flen < "3"){
	  flag1 = false; 
	    document.getElementById('nameerror').innerHTML = 'Please Enter Valid Name.';
	  }
	  else {
	  	flag1 = true;
	    document.getElementById('nameerror').innerHTML = '';
	  }
	}

//Function for Email Address validation
	function email_name(){ 
	  var emailname = document.getElementById('email').value;
	  var email_pattern = /^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,4}$/;
	  if(emailname==null || emailname==""){
	  	flag2 = false;
	    document.getElementById('emailerror').innerHTML ='Please Enter The Email.';
	  }
	  else if(!email_pattern.test(emailname)){
	  	flag2 = false;
	    document.getElementById('emailerror').innerHTML ='Please Enter Valid Email.';
	  }
	  else {
	  	flag2 = true;
	    document.getElementById('emailerror').innerHTML = '';
	  }
	}

//Function for Mobile validation
	function tele_num(){
	  var tname = document.getElementById('mobile').value;
	  var num = /^(([0-9]|[\+])?)(([0-9]|[\-])?)+$/;
	  var tnamenum= tname.length;
	  if (tname==null || tname=="" ){
	  	flag3 = false;
	    document.getElementById('moberror').innerHTML ='Please Enter The Mobile Number.';
	  } 
	  else if(!num.test(tname) || tnamenum < "10"){
	  	flag3 = false;
	    document.getElementById('moberror').innerHTML ='Please Enter Valid Mobile Number.';
	  }
	  else {
	  	flag3 = true;
	    document.getElementById('moberror').innerHTML = '';
	  }
	}

//Function for Pincode validation
	function pin_code(){
	  var pcode = document.getElementById('pincode').value;
	  var pnum = /^[0-9]+$/;
	  var pcodenum= pcode.length;
	  if (pcode==null || pcode=="" ){
	  	flag4 = false;
	    document.getElementById('pinerror').innerHTML ='Please Enter The Number.';
	  } 
	  else if(!pnum.test(pcode) || pcodenum < "6"){
	  	flag4 = false;
	    document.getElementById('pinerror').innerHTML ='Please Enter Valid Number.';
	  }
	  else {
	  	flag4 = true;
	    document.getElementById('pinerror').innerHTML = '';
	  }
	}

//Function for User Name validation
	function user_name(){
	  var uname = document.getElementById('username').value;
	  var ulen = uname.length;
	  var uname_pattern = /^[a-zA-Z\s\.\-]+[a-zA-Z\.\']+$/;
	  if (uname==null || uname=="" ){
	  	flag5 = false;
	    document.getElementById('usererror').innerHTML ='Please Enter The Name.'; 
	  }
	  else if(!uname_pattern.test(uname) || ulen < "3"){ 
	  	flag5 = false;
	    document.getElementById('usererror').innerHTML ='Please Enter Valid Name.';
	  }
	  else {
	  	flag5 = true;
	    document.getElementById('usererror').innerHTML = '';
	  }
	}

//Function for Password validation
	function vali_password(){
	  var pass = document.getElementById('password').value;
	  var plen = pass.length;
	  var pass_pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}/;
	  if (pass==null || pass=="" ){
	  	flag6 = false;
	    document.getElementById('passerror').innerHTML = 'Please Enter The Password.'; 
	  }
	  else if(!pass_pattern.test(pass) || plen < "6"){ 
	  	flag6 = false;
	    document.getElementById('passerror').innerHTML = 'Please Enter Valid Password (Note: password should be one uppercase, one lowercase, one special character and one numeric character).';
	  }
	  else {
	  	flag6 = true;
	    document.getElementById('passerror').innerHTML = '';
	  }
	}

	//Function for Confirm Password validation
	function vali_Cpassword(){
	  var cpass = document.getElementById('conf-password').value;
	  var pass = document.getElementById('password').value;
	  if (cpass==null || cpass=="" ){
	  	flag7 = false;
	    document.getElementById('cpasserror').innerHTML = 'Please Enter The Password.'; 
	  }
	  else if(cpass !== pass){ 
	  	flag7 = false;
	    document.getElementById('cpasserror').innerHTML = 'Please Enter Correct Password.';
	  }
	  else {
	  	flag7 = true;
	    document.getElementById('cpasserror').innerHTML = '';
	  }
	}

	//Function for Confirm Password validation
	function vali_cqbox(){
	  var chqbox = document.getElementById('check-box');
	   if(!chqbox.checked){
	   	flag8 = false;
    	document.getElementById('checkerror').innerHTML = 'You must agree to the terms first.';
    	
		}
	  else {
	  	flag7 = true;
	    document.getElementById('checkerror').innerHTML = '';
	  }
	}

	// function registration(){
	// 	alert("You Register succesfully");
	// 	location.reload();
	// }

};