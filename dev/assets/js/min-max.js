/**********Maximum and Minimum Number using function**********/
var array = [1,2,3,4,5,4,3,2,1];
var a = Math.max.apply(Math,array); 
var b = Math.min.apply(Math,array);
console.log("maximum: " +a);
console.log("minimum: " +b);

/**********Maximum and Minimum Number without using function**********/
var arr = [1,2,3,4,5,4,3,2,1];
var max, min;
for(i=1;i<arr.length;++i)  
  {
    if(arr[0]<arr[i]) 
      {
        arr[0]=arr[i];
        max=arr[0];
      }
    else 
      {
        arr[0]=arr[i];
        min=arr[0];
      }
  }
document.write("Array is: " +arr +"<br/>");
document.write("Maximum no. is: " +max +"<br/>");
document.write("Minimum no. is: " +min +"<br/><br/>");

/**********Ascending Order**********/
var arr = [1,4,5,3,2,6,10,8,7,9];
document.write("Array is: " +arr +"<br/>");
var temp;
var asce = [];
for (i = 0; i < arr.length; i++)
{
  for (j = i + 1; j < arr.length; j++)
  {
    if (arr[i] > arr[j])
    {
      temp =  arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }
  }
  asce[i] = arr[i];
}
document.write("Ascending order is: " +asce +"<br/>");

/**********Dscending Order**********/
var arr = ["Mango","Orange","Apple","Banana","Grapes"];
document.write("Array is: " +arr +"<br/>");
var temp;
var dsce = [];
for (i = 0; i < arr.length; i++)
{
  for (j = i + 1; j < arr.length; j++)
  {
    if (arr[i] < arr[j])
    {
      temp =  arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }
  }
  dsce[i] = arr[i];
}
document.write("Dscending order is: " +dsce +"<br/><br/>");

/**********Find position of Char in given string**********/
var str = "Hello! I am sachin";
var pos = [];
for (i = 0; i < str.length; i++)
  {
    if (str[i] == 'a')
      {
        pos[pos.length] = i;
      }
  }
document.write("String is: " +str +"<br/>");
document.write("Position of a is: " +pos +"<br/><br/>");