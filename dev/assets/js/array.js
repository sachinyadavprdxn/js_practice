/********Array**********/
document.write("********Array Methods********** <br/>");

var colors = ["red", "blue", "green"];
document.write(colors[2]);
document.write("<br />");

var colors = ["red", "blue", "green"];
var name = [];
document.write(colors.length);
document.write("<br />");
document.write(name.length);
document.write("<br />");

var numbers = [1, 2, 3, 4, 5];
for (var i = 0; i < numbers.length; i++) {
  document.write(numbers[i] *= 2);
}
document.write("<br/><br/>"); 

/********Push**********/

document.write("********Push Methods********** <br/>");
var weeks = ['sun', 'mon', 'tue'];
document.write( "Befor: " + weeks +"<br/>");
weeks[weeks.length] = "Wed, Thru";
document.write( "After: " + weeks );

document.write("<br/><br/>"); 

/********Pop**********/

document.write("********Pop Methods********** <br/>");
var weeks = ['sun', 'mon', 'tue', 'wed'];
document.write("Befor Pop: " +weeks +"<br/>");
weeks.length = weeks.length - 1;
document.write("After: " +weeks );

document.write("<br/><br/>"); 

/********Reverse**********/
document.write("********Reverse Methods********** <br/>");
var numbers = [1, 2, 3, 4, 5];
document.write("Befor: " +numbers +"<br/>");
var num = [];
for (var i = 0; i <= numbers.length; i++) {
  num[i - 1] = numbers[numbers.length - i];
}
document.write("After: " +num);
document.write("<br/>"); 

var numbers = ["sun\n", "mon\n", "tue\n", "wed\n", "thru\n"];

for (var i = 0; i < numbers.length; i++) {
var str = numbers[i];
var str2 = (str.split("").reverse().join("").split(" ").reverse().join(" "));
  document.write(str2);
}
document.write("<br/>"); 

var numbers = ["sun\n", "mon\n", "tue\n", "wed\n", "thru\n"];

for (var i = numbers.length; i > 0; i--) {
var str = numbers[i-1];
var str2 = (str.split("").reverse().join("").split(" ").reverse().join(" "));
  document.write(str2);
}
document.write("<br/><br/>"); 

/********Shift**********/
document.write("********Shift Methods********** <br/>");
var myFish = ['angel', 'clown', 'mandarin', 'surgeon'];

document.write('myFish before: ' + myFish);
var shifted = myFish.shift(); 
document.write('myFish after: ' + myFish); 
document.write('Removed this element: ' + shifted); 
document.write("<br/>"); 

/********Shift without shift**********/
document.write("********Shift without shift********** <br/>");
var colors = ["red", "green", "blue", "yellow"];
document.write("Befor: " +colors +"<br/>");
var temp;
for (var i = colors.length - 1; i > 0; i--) {
  temp = colors[i]; 
  colors[i] = colors[0]; 
  colors[0] = temp;
}
colors.length = colors.length - 1;
document.write("After: " +colors);
document.write("<br/><br/>"); 


/********UnShift**********/
document.write("********UnShift Methods********** <br/>");
var arr = [1, 2];
document.write('before: ' + arr +"\n");
arr.unshift(0); 
document.write('after: ' + arr +"\n");
arr.unshift(-2, -1); 
document.write('after: ' + arr);
document.write("<br/>"); 

/********UnShift without unshift**********/
document.write("********UnShift without unshift********** <br/>");
var colors = ["red", "green", "blue"];
document.write("Befor: " +colors +"<br/>");
var temp;
colors[colors.length] = "yellow";
for (var i = 0; i < colors.length; i++) {
  temp = colors[i]; 
  colors[i] = colors[colors.length - 1]; 
  colors[colors.length - 1] = temp;
}
document.write("After: " +colors);
document.write("<br/><br/>"); 

/********Manipulation**********/
document.write("********Manipulation Methods********** <br/>");
var colors = ["red", "green", "blue"];
document.write("Before Concat: " +colors +"<br/>");
var colors2 = colors.concat("yellow", ["black", "brown"]);
document.write("After Concat: " +colors2 +"<br/");

document.write("********Slice Methods********** <br/>");
var colors = ["red", "green", "blue", "yellow", "black", "brown"];
document.write("Initial Array: " +colors +"<br/>");
var colors2 = colors.slice(1);
document.write("After slice from position 1: " +colors2 +"<br/>");
var colors3 = colors.slice(1,4);
document.write("slicing between position 1-4: " +colors3 +"<br/>");

document.write("********Splice Methods********** <br/>");
var colors = ["red", "green", "blue"];
var removed = colors.splice(0,1);
document.write("Remove the first item: " +colors +"<br/>");
document.write("Removed: " +removed +"<br/>");

removed = colors.splice(1, 0, "yellow", "orange"); 
document.write("Insert two items at position 1: " +colors +"<br/>");

removed = colors.splice(1, 1, "red", "purple");
document.write("Insert two values, remove one: " +colors +"<br/>");
document.write("Removed: " +removed +"<br/><br/>");

/********Location**********/
document.write("********Location Methods********** <br/>");
var numbers = [1,2,3,4,5,4,3,2,1];
document.write("Array: " +numbers +"<br/>"); 
document.write("indexOf(3): " +numbers.indexOf(3) +"<br/>");
document.write("indexOf(3): " +numbers.lastIndexOf(3) +"<br/>");

document.write("indexOf(3, 2): " +numbers.indexOf(3, 2) +"<br/>");

document.write("indexOf(3, 6): " +numbers.lastIndexOf(3, 6) +"<br/>"); 
var person = { name: "“Nicholas”" };
var people = [{ name: "“Nicholas”" }];
var morePeople = [person];
document.write(people.indexOf(person) +"<br/>");
document.write(morePeople.indexOf(person) +"<br/><br/>");

/********Iterative**********/
document.write("********Iterative Methods********** <br/>");
document.write("********Every Methods********** <br/>");
var numbers = [1,2,3,4,5,4,3,2,1];
document.write("Array: " +numbers +"<br/>");
var everyResult = numbers.every(function(item, index, array){
return (item > 2);
});
document.write(everyResult +"<br/>");

var numbers = [3,5,3,4,5,4,3,5,3];
var everyResult = numbers.every(function(item, index, array){
return (item > 2);
});
document.write(everyResult +"<br/>");

document.write("********Some Methods********** <br/>");
var numbers = [1,2,3,4,5,4,3,2,1];
document.write("Array: " +numbers +"<br/>");
var someResult = numbers.some(function(item, index, array){
return (item > 2);
});
document.write(someResult +"<br/>");

var numbers = [1,2,3,4,5,4,3,2,1];
var someResult = numbers.some(function(item, index, array){
return (item > 6);
});
document.write(someResult +"<br/>");

document.write("********Filter Methods********** <br/>");
var arr = [
    {"name":"apple", "count": 2},
    {"name":"orange", "count": 5},
    {"name":"pear", "count": 3},
    {"name":"orange", "count": 16}
];
document.write("Array: ");
for(var i=0; i<arr.length;i++){

  document.write("{Name: " + arr[i].name + ", Count: " + arr[i].count +"}, "); 
}
var newArr = arr.filter(function(item){
    return item.name == "orange";
});
for(var i=0; i<newArr.length;i++){
document.write("<br/>Filter results: Name: " + newArr[i].name + ", Count: " + arr[i].count +" ");
}

document.write("<br/>********Foreach Methods********** <br/>");
var numbers = [1,2,3,4,5,4,3,2,1];
document.write("Array: " +numbers +"<br/>");
numbers.forEach(function(item, index, array){
document.write("[" + index + "] is " + item +"<br/>");
});
